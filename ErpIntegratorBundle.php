<?php

namespace Officient\ErpIntegrator;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ErpIntegratorBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\ErpIntegrator
 */
class ErpIntegratorBundle extends Bundle
{
    //
}