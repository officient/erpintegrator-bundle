<?php

namespace Officient\ErpIntegrator;

use Officient\ErpIntegrator\Exception\ConnectionException;
use Officient\ErpIntegrator\Exception\ERPintegratorException;
use Officient\ErpIntegrator\Exception\ResponseDecodeException;

class Client implements ClientInterface
{
    /**
     * @var string|null
     */
    private $host;

    /**
     * @var int|null
     */
    private $port;

    /**
     * @param string|null $host
     * @param int|null $port
     */
    public function __construct(?string $host, ?int $port)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @inheritDoc
     * @throws ConnectionException
     * @throws ResponseDecodeException
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response
    {
        if(empty($this->host)) {
            throw new ConnectionException("Host is missing from config");
        }

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $this->host.$query);
        if(!is_null($this->port)) {
            curl_setopt($handle, CURLOPT_PORT, $this->port);
        }
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        if($method !== self::METHOD_GET) {
            if(!is_null($data)) {
                $postFields = json_encode($data);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $postFields);
            }
        }

        $headers = array();
        curl_setopt($handle, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers) {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) // ignore invalid headers
                return $len;

            $headers[strtolower(trim($header[0]))][] = trim($header[1]);

            return $len;
        });

        $content = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        $response = json_decode($content, true);

        if($httpCode === 0) {
            throw new ConnectionException("Could not connect to ERPintegrator");
        } else if(!in_array($httpCode, [200, 201, 404])) {
            throw new ERPintegratorException("Error when calling ERPintegrator (HttpCode: $httpCode, Content: $content)");
        }

        if(!is_array($response)) {
            throw new ResponseDecodeException("Failed decoding response (content: $content)");
        }

        return new Response($httpCode, $response, $headers);
    }
}