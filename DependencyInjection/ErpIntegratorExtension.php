<?php

namespace Officient\ErpIntegrator\DependencyInjection;

use Officient\ErpIntegrator\DependencyInjection\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ErpIntegratorExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $clientDef = $container->getDefinition('Officient\ErpIntegrator\Client');
        $clientDef->replaceArgument(0, $config['host'] ?? null);
        $clientDef->replaceArgument(1, $config['port'] ?? null);
    }
}