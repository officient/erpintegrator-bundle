<?php

namespace Officient\ErpIntegrator\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('erp_integrator');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('host')->isRequired()->end()
                ->integerNode('port')->isRequired()->end()
            ->end();

        return $treeBuilder;
    }
}