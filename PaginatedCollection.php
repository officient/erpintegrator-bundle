<?php

namespace Officient\ErpIntegrator;

class PaginatedCollection extends Collection
{
    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var int
     */
    protected $totalPages;

    /**
     * @var int
     */
    protected $itemsPerPage;

    public function __construct(int $currentPage, int $totalPages, int $itemsPerPage, array $items = array())
    {
        parent::__construct($items);
        $this->currentPage = $currentPage;
        $this->totalPages = $totalPages;
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }
}