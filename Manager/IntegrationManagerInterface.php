<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Entity\Integration;

interface IntegrationManagerInterface
{
    /**
     * @param int|null $companyId
     * @param bool $includeDeleted
     * @return Collection
     */
    public function findAll(?int $companyId = null, bool $includeDeleted = false): Collection;

    /**
     * @param int $id
     * @param int|null $companyId
     * @param bool $includeDeleted
     * @return Integration|null
     */
    public function find(int $id, ?int $companyId = null, bool $includeDeleted = false): ?Integration;

    /**
     * @param string $name
     * @param int $companyId
     * @param string $type
     * @param bool $active
     * @return Integration|null
     */
    public function create(string $name, int $companyId, string $type, bool $active): ?Integration;

    /**
     * @param Integration $integration
     * @param int|null $companyId
     * @param bool $includeDeleted
     * @return Integration|null
     */
    public function update(Integration $integration, ?int $companyId = null, bool $includeDeleted = false): ?Integration;

    /**
     * @param Integration $integration
     * @param int|null $companyId
     * @return bool
     */
    public function delete(Integration $integration, ?int $companyId = null): bool;
}