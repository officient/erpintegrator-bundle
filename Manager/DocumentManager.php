<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\ClientInterface;

class DocumentManager extends AbstractManager implements DocumentManagerInterface
{
    public function document(string $name, string $type, int $companyId, string $base64Content, string $flowUuid, ?string $md5 = null, ?array $metadata = null): bool
    {
        $fields = [
            'name' => $name,
            'type' => $type,
            'companyId' => $companyId,
            'base64Content' => $base64Content,
            'flowUuid' => $flowUuid,
            'md5' => $md5,
            'metadata' => $metadata
        ];

        $response = $this->client->doRequest('/internal/documents', $fields, ClientInterface::METHOD_POST);
        return $response->getHttpCode() === 200;
    }
}