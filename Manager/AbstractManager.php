<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\ClientInterface;

abstract class AbstractManager
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
}