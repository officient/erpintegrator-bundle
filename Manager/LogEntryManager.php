<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Entity\LogEntry;
use Officient\ErpIntegrator\PaginatedCollection;

class LogEntryManager extends AbstractManager implements LogEntryManagerInterface
{
    /**
     * @inheritDoc
     */
    public function findAll(
        ?int $integrationId = null,
        ?int $documentId = null,
        ?string $locale = null,
        ?string $order = null,
        ?string $orderDir = null,
        ?string $searchQuery = null
    ): Collection
    {
        $query = '/internal/log_entries';
        $params = array();
        if($integrationId) {
            $params[] = "integrationId=$integrationId";
        }
        if($documentId) {
            $params[] = "documentId=$documentId";
        }
        if($locale) {
            $params[] = "locale=$locale";
        }
        if($searchQuery) {
            $params[] = "searchQuery=$searchQuery";
        }
        if($order) {
            $params[] = "order=$order";
        }
        if($orderDir) {
            $params[] = "orderDir=$orderDir";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $result = array();
        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            foreach ($response->getContent()['data'] as $value) {
                $result[] = $this->createLogEntry($value);
            }
        }

        return new Collection($result);
    }

    /**
     * @inheritDoc
     */
    public function findAllPaginated(
        int $page,
        int $perPage = 10,
        ?int $integrationId = null,
        ?int $documentId = null,
        ?string $locale = null,
        ?string $order = null,
        ?string $orderDir = null,
        ?string $searchQuery = null
    ): PaginatedCollection
    {
        $query = '/internal/log_entries';
        $params = array();
        if($integrationId) {
            $params[] = "integrationId=$integrationId";
        }
        if($documentId) {
            $params[] = "documentId=$documentId";
        }
        if($locale) {
            $params[] = "locale=$locale";
        }
        if($searchQuery) {
            $params[] = "searchQuery=$searchQuery";
        }
        if($order) {
            $params[] = "order=$order";
        }
        if($orderDir) {
            $params[] = "orderDir=$orderDir";
        }
        if($page) {
            $params[] = "page=$page";
        }
        if($perPage) {
            $params[] = "perPage=$perPage";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $result = array();
        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            foreach ($response->getContent()['data'] as $value) {
                $result[] = $this->createLogEntry($value);
            }
        }

        $attributes = $response->getContent()['attributes'] ?? [];
        $currentPage = $attributes['currentPage'] ?? 0;
        $totalPages = $attributes['totalPages'] ?? 0;
        $itemsPerPage = $attributes['itemsPerPage'] ?? 0;

        return new PaginatedCollection($currentPage, $totalPages, $itemsPerPage, $result);
    }

    /**
     * @inheritDoc
     */
    public function find(int $id, ?int $integrationId = null, ?int $documentId = null, ?string $locale = null): ?LogEntry
    {
        $query = '/internal/log_entries/'.$id;
        $params = array();
        if($integrationId) {
            $params[] = "integrationId=$integrationId";
        }
        if($documentId) {
            $params[] = "documentId=$documentId";
        }
        if($locale) {
            $params[] = "locale=$locale";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            return $this->createLogEntry($value);
        } else {
            return null;
        }
    }

    private function createLogEntry(array $value): LogEntry
    {
        return (new LogEntry())
            ->setId($value['id'])
            ->setMessage($value['message'])
            ->setMessageTranslated($value['messageTranslated'])
            ->setType($value['type'])
            ->setTypeTranslated($value['typeTranslated'])
            ->setCreatedDatetime(new \DateTime($value['createdDatetime']))
            ->setDocumentName($value['documentName']);
    }
}