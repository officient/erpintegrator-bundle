<?php

namespace Officient\ErpIntegrator\Manager;

interface DocumentManagerInterface
{
    /**
     * @param string $name
     * @param string $type
     * @param int $companyId
     * @param string $base64Content
     * @param string $flowUuid
     * @param string|null $md5
     * @param array|null $metadata
     * @return bool
     */
    public function document(string $name, string $type, int $companyId, string $base64Content, string $flowUuid, ?string $md5 = null, ?array $metadata = null): bool;
}