<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Entity\Integration;
use Officient\ErpIntegrator\Entity\Webhook;

interface WebhookManagerInterface
{
    /**
     * @param int|null $companyId
     * @param string|null $type
     * @param string|null $networkType
     * @param string|null $networkSchemeId
     * @param string|null $networkParticipantId
     * @param bool $includeDeleted
     * @return Collection
     */
    public function findAll(
        ?int $companyId = null,
        ?string $type = null,
        ?string $networkType = null,
        ?string $networkSchemeId = null,
        ?string $networkParticipantId = null,
        bool $includeDeleted = false
    ): Collection;

    /**
     * @param int $id
     * @param int|null $companyId
     * @param bool $includeDeleted
     * @return Webhook|null
     */
    public function find(int $id, ?int $companyId = null, bool $includeDeleted = false): ?Webhook;

    /**
     * @param int $companyId
     * @param string $type
     * @param bool $active
     * @param string $callbackUrl
     * @param string|null $callbackAuthMethod
     * @param string|null $callbackAuthParam1
     * @param string|null $callbackAuthParam2
     * @param string|null $networkType
     * @param string|null $networkSchemeId
     * @param string|null $networkParticipantId
     * @return Webhook|null
     */
    public function create(
        int $companyId,
        string $type,
        bool $active,
        string $callbackUrl,
        ?string $callbackAuthMethod = null,
        ?string $callbackAuthParam1 = null,
        ?string $callbackAuthParam2 = null,
        ?string $networkType = null,
        ?string $networkSchemeId = null,
        ?string $networkParticipantId = null
    ): ?Webhook;

    /**
     * @param Webhook $webhook
     * @param int|null $companyId
     * @param bool $includeDeleted
     * @return Integration|null
     */
    public function update(Webhook $webhook, ?int $companyId = null, bool $includeDeleted = false): ?Webhook;

    /**
     * @param Webhook $webhook
     * @param int|null $companyId
     * @return bool
     */
    public function delete(Webhook $webhook, ?int $companyId = null): bool;
}