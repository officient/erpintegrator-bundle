<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Entity\LogEntry;
use Officient\ErpIntegrator\PaginatedCollection;

interface LogEntryManagerInterface
{
    /**
     * @param int|null $integrationId
     * @param int|null $documentId
     * @param string|null $locale
     * @param string|null $order
     * @param string|null $orderDir
     * @param string|null $searchQuery
     * @return Collection
     */
    public function findAll(
        ?int $integrationId = null,
        ?int $documentId = null,
        ?string $locale = null,
        ?string $order = null,
        ?string $orderDir = null,
        ?string $searchQuery = null
    ): Collection;

    /**
     * @param int $page
     * @param int $perPage
     * @param int|null $integrationId
     * @param int|null $documentId
     * @param string|null $locale
     * @param string|null $order
     * @param string|null $orderDir
     * @param string|null $searchQuery
     * @return PaginatedCollection
     */
    public function findAllPaginated(
        int $page,
        int $perPage = 10,
        ?int $integrationId = null,
        ?int $documentId = null,
        ?string $locale = null,
        ?string $order = null,
        ?string $orderDir = null,
        ?string $searchQuery = null
    ): PaginatedCollection;

    /**
     * @param int $id
     * @param int|null $integrationId
     * @param int|null $documentId
     * @param string|null $locale
     * @return LogEntry|null
     */
    public function find(int $id, ?int $integrationId = null, ?int $documentId = null, ?string $locale = null): ?LogEntry;
}