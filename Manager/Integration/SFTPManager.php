<?php

namespace Officient\ErpIntegrator\Manager\Integration;

use Officient\ErpIntegrator\ClientInterface;
use Officient\ErpIntegrator\Entity\Integration;
use Officient\ErpIntegrator\Exception\ERPintegratorException;
use Officient\ErpIntegrator\Manager\AbstractManager;

class SFTPManager extends AbstractManager implements SFTPManagerInterface
{
    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function delete(Integration $integration, ?int $companyId = null, bool $includeDeleted = false): bool
    {
        if(is_null($integration->getId())) {
            throw new ERPintegratorException("Can not update integration without id");
        }

        if($integration->getType() !== Integration::TYPE_SFTP) {
            throw new ERPintegratorException("Integration is not of type ".Integration::TYPE_SFTP);
        }

        $query = '/internal/integrations/'.$integration->getId().'/sftp';
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query, null, ClientInterface::METHOD_DELETE);
        return $response->getHttpCode() === 200;
    }

    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function undelete(Integration $integration, ?int $companyId = null, bool $includeDeleted = false): bool
    {
        if(is_null($integration->getId())) {
            throw new ERPintegratorException("Can not update integration without id");
        }

        if($integration->getType() !== Integration::TYPE_SFTP) {
            throw new ERPintegratorException("Integration is not of type ".Integration::TYPE_SFTP);
        }

        $query = '/internal/integrations/'.$integration->getId().'/sftp';
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query, null, ClientInterface::METHOD_POST);
        return $response->getHttpCode() === 200;
    }
}