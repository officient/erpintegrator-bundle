<?php

namespace Officient\ErpIntegrator\Manager\Integration;

use Officient\ErpIntegrator\Entity\Integration;

interface SFTPManagerInterface
{
    /**
     * Delete an integration SFTP user
     * This is a soft delete. This will not delete the integration or set it inactive.
     * @param Integration $integration
     * @return bool
     */
    public function delete(Integration $integration): bool;

    /**
     * Undelete an integration SFTP user
     * This will undelete a previously deleted SFTP user.
     * @param Integration $integration
     * @return bool
     */
    public function undelete(Integration $integration): bool;
}