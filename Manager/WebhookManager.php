<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\ClientInterface;
use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Entity\Webhook;
use Officient\ErpIntegrator\Exception\ERPintegratorException;

class WebhookManager extends AbstractManager implements WebhookManagerInterface
{
    public function findAll(
        ?int $companyId = null,
        ?string $type = null,
        ?string $networkType = null,
        ?string $networkSchemeId = null,
        ?string $networkParticipantId = null,
        bool $includeDeleted = false
    ): Collection
    {
        $query = '/internal/webhooks';
        $params = array();

        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        if($type) {
            $params[] = "type=$type";
        }

        if($networkType) {
            $params[] = "networkType=$networkType";
        }

        if($networkSchemeId) {
            $params[] = "networkSchemeId=$networkSchemeId";
        }

        if($networkParticipantId) {
            $params[] = "networkParticipantId=$networkParticipantId";
        }

        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $result = array();
        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            foreach ($response->getContent()['data'] as $value) {
                $result[] = (new Webhook())
                    ->setId($value['id'])
                    ->setCompanyId($value['companyId'])
                    ->setType($value['type'])
                    ->setActive($value['active'])
                    ->setCallbackUrl($value['callbackUrl'])
                    ->setCallbackAuthMethod($value['callbackAuthMethod'])
                    ->setCallbackAuthParam1($value['callbackAuthParam1'])
                    ->setCallbackAuthParam2($value['callbackAuthParam2'])
                    ->setNetworkType($value['networkType'])
                    ->setNetworkSchemeId($value['networkSchemeId'])
                    ->setNetworkParticipantId($value['networkParticipantId'])
                    ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                    ->setCreatedDatetime(new \DateTime($value['createdDatetime']));
            }
        }
        return new Collection($result);
    }

    public function find(int $id, ?int $companyId = null, bool $includeDeleted = false): ?Webhook
    {
        $query = '/internal/webhooks/'.$id;
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            return (new Webhook())
                ->setId($value['id'])
                ->setCompanyId($value['companyId'])
                ->setType($value['type'])
                ->setActive($value['active'])
                ->setCallbackUrl($value['callbackUrl'])
                ->setCallbackAuthMethod($value['callbackAuthMethod'])
                ->setCallbackAuthParam1($value['callbackAuthParam1'])
                ->setCallbackAuthParam2($value['callbackAuthParam2'])
                ->setNetworkType($value['networkType'])
                ->setNetworkSchemeId($value['networkSchemeId'])
                ->setNetworkParticipantId($value['networkParticipantId'])
                ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                ->setCreatedDatetime(new \DateTime($value['createdDatetime']));
        } else {
            return null;
        }
    }

    public function create(
        int $companyId,
        string $type,
        bool $active,
        string $callbackUrl,
        ?string $callbackAuthMethod = null,
        ?string $callbackAuthParam1 = null,
        ?string $callbackAuthParam2 = null,
        ?string $networkType = null,
        ?string $networkSchemeId = null,
        ?string $networkParticipantId = null
    ): ?Webhook
    {
        $fields = [
            'companyId' => $companyId,
            'type' => $type,
            'active' => $active,
            'callbackUrl' => $callbackUrl,
            'callbackAuthMethod' => $callbackAuthMethod,
            'callbackAuthParam1' => $callbackAuthParam1,
            'callbackAuthParam2' => $callbackAuthParam2,
            'networkType' => $networkType,
            'networkSchemeId' => $networkSchemeId,
            'networkParticipantId' => $networkParticipantId
        ];

        $response = $this->client->doRequest('/internal/webhooks', $fields, ClientInterface::METHOD_POST);
        if($response->getHttpCode() === 201 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            return (new Webhook())
                ->setId($value['id'])
                ->setCompanyId($value['companyId'])
                ->setType($value['type'])
                ->setActive($value['active'])
                ->setCallbackUrl($value['callbackUrl'])
                ->setCallbackAuthMethod($value['callbackAuthMethod'])
                ->setCallbackAuthParam1($value['callbackAuthParam1'])
                ->setCallbackAuthParam2($value['callbackAuthParam2'])
                ->setNetworkType($value['networkType'])
                ->setNetworkSchemeId($value['networkSchemeId'])
                ->setNetworkParticipantId($value['networkParticipantId'])
                ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                ->setCreatedDatetime(new \DateTime($value['createdDatetime']));
        } else {
            return null;
        }
    }

    public function update(Webhook $webhook, ?int $companyId = null, bool $includeDeleted = false): ?Webhook
    {
        if(is_null($webhook->getId())) {
            throw new ERPintegratorException("Can not update webhook without id");
        }

        $query = '/internal/webhooks/'.$webhook->getId();
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $fields = [
            'companyId' => $webhook->getCompanyId(),
            'type' => $webhook->getType(),
            'active' => $webhook->isActive(),
            'callbackUrl' => $webhook->getCallbackUrl(),
            'callbackAuthMethod' => $webhook->getCallbackAuthMethod(),
            'callbackAuthParam1' => $webhook->getCallbackAuthParam1(),
            'callbackAuthParam2' => $webhook->getCallbackAuthParam2(),
            'networkType' => $webhook->getNetworkType(),
            'networkSchemeId' => $webhook->getNetworkSchemeId(),
            'networkParticipantId' => $webhook->getNetworkParticipantId()
        ];

        $response = $this->client->doRequest($query, $fields, ClientInterface::METHOD_PUT);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            $webhook->setUpdatedDatetime(new \DateTime($value['updatedDatetime']));
            return $webhook;
        } else {
            return null;
        }
    }

    public function delete(Webhook $webhook, ?int $companyId = null): bool
    {
        if(is_null($webhook->getId())) {
            throw new ERPintegratorException("Can not delete webhook without id");
        }

        $query = '/internal/webhooks/'.$webhook->getId();
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query, null, ClientInterface::METHOD_DELETE);
        return $response->getHttpCode() === 200;
    }
}