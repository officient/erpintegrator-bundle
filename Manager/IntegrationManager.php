<?php

namespace Officient\ErpIntegrator\Manager;

use Officient\ErpIntegrator\ClientInterface;
use Officient\ErpIntegrator\Collection;
use Officient\ErpIntegrator\Exception\ERPintegratorException;
use Officient\ErpIntegrator\Entity\Integration;

class IntegrationManager extends AbstractManager implements IntegrationManagerInterface
{
    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function findAll(?int $companyId = null, bool $includeDeleted = false): Collection
    {
        $query = '/internal/integrations';
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $result = array();
        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            foreach ($response->getContent()['data'] as $value) {
                $result[] = (new Integration())
                    ->setId($value['id'])
                    ->setName($value['name'])
                    ->setCompanyId($value['companyId'])
                    ->setType($value['type'])
                    ->setActive($value['active'])
                    ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                    ->setCreatedDatetime(new \DateTime($value['createdDatetime']))
                    ->setApiKey($value['apiKey'])
                    ->setUsername($value['username'])
                    ->setPassword($value['password'])
                    ->setLastPingDatetime((!is_null($value['lastPingDatetime']) ? new \DateTime($value['lastPingDatetime']) : null));
            }
        }
        return new Collection($result);
    }

    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function find(int $id, ?int $companyId = null, bool $includeDeleted = false): ?Integration
    {
        $query = '/internal/integrations/'.$id;
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            return (new Integration())
                ->setId($value['id'])
                ->setName($value['name'])
                ->setCompanyId($value['companyId'])
                ->setType($value['type'])
                ->setActive($value['active'])
                ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                ->setCreatedDatetime(new \DateTime($value['createdDatetime']))
                ->setApiKey($value['apiKey'])
                ->setUsername($value['username'])
                ->setPassword($value['password'])
                ->setLastPingDatetime((!is_null($value['lastPingDatetime']) ? new \DateTime($value['lastPingDatetime']) : null));
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function create(string $name, int $companyId, string $type, bool $active): ?Integration
    {
        $fields = [
            'name' => $name,
            'companyId' => $companyId,
            'type' => $type,
            'active' => $active
        ];

        $response = $this->client->doRequest('/internal/integrations', $fields, ClientInterface::METHOD_POST);
        if($response->getHttpCode() === 201 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            return (new Integration())
                ->setId($value['id'])
                ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
                ->setCreatedDatetime(new \DateTime($value['createdDatetime']))
                ->setApiKey($value['apiKey'])
                ->setUsername($value['username'])
                ->setPassword($value['password'])
                ->setLastPingDatetime((!is_null($value['lastPingDatetime']) ? new \DateTime($value['lastPingDatetime']) : null));
        } else {
            return null;
        }
    }


    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function update(Integration $integration, ?int $companyId = null, bool $includeDeleted = false): ?Integration
    {
        if(is_null($integration->getId())) {
            throw new ERPintegratorException("Can not update integration without id");
        }

        $query = '/internal/integrations/'.$integration->getId();
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        $params[] = "includeDeleted=".($includeDeleted ? "1" : "0");
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $fields = [
            'name' => $integration->getName(),
            'companyId' => $integration->getCompanyId(),
            'type' => $integration->getType(),
            'active' => $integration->isActive()
        ];

        $response = $this->client->doRequest($query, $fields, ClientInterface::METHOD_PUT);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data'])) {
            $value = $response->getContent()['data'];
            $integration->setUpdatedDatetime(new \DateTime($value['updatedDatetime']));
            return $integration;
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     * @throws ERPintegratorException
     */
    public function delete(Integration $integration, ?int $companyId = null): bool
    {
        if(is_null($integration->getId())) {
            throw new ERPintegratorException("Can not delete integration without id");
        }

        $query = '/internal/integrations/'.$integration->getId();
        $params = array();
        if($companyId) {
            $params[] = "companyId=$companyId";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $response = $this->client->doRequest($query, null, ClientInterface::METHOD_DELETE);
        return $response->getHttpCode() === 200;
    }
}