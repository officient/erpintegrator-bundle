<?php

namespace Officient\ErpIntegrator;

class Collection implements \ArrayAccess, \Iterator, \Countable, \JsonSerializable
{
    /**
     * @var array
     */
    protected $container;

    /**
     * @var array
     */
    protected $keys;

    /**
     * @var int
     */
    protected $position;

    public function __construct(array $items = array())
    {
        $this->container = $items;
        $this->keys = array_keys($items);
        $this->position = 0;
    }

    public function all(): array
    {
        return $this->container;
    }

    public function add($value): void
    {
        $this->offsetSet(null, $value);
    }

    public function set($key, $value): void
    {
        $this->offsetSet($key, $value);
    }

    public function get($key): mixed
    {
        return $this->offsetGet($key);
    }

    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current(): mixed
    {
        return $this->container[$this->keys[$this->position]];
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next(): void
    {
        ++$this->position;
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return string|float|int|bool|null scalar on success, or null on failure.
     */
    public function key(): mixed
    {
        return $this->keys[$this->position];
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool
    {
        return isset($this->keys[$this->position]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset): mixed
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if(is_null($offset)) {
            $this->container[] = $value;
            $this->keys[] = array_key_last($this->container);
        } else {
            $this->container[$offset] = $value;
            if(!in_array($offset, $this->keys)) $this->keys[] = $offset;
        }
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
        unset($this->keys[array_search($offset,$this->keys)]);
        $this->keys = array_values($this->keys);
    }

    /**
     * Count elements of an object
     * @link https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * <p>
     * The return value is cast to an integer.
     * </p>
     */
    public function count(): int
    {
        return count($this->keys);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4
     */
    public function jsonSerialize(): mixed
    {
        return $this->container;
    }
}