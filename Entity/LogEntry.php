<?php

namespace Officient\ErpIntegrator\Entity;

class LogEntry implements \JsonSerializable
{
    const TYPE_INFO             = 'info';
    const TYPE_ERROR            = 'error';

    const TYPES                 = [
        self::TYPE_INFO,
        self::TYPE_ERROR
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string|null
     */
    private $messageTranslated;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $typeTranslated;

    /**
     * @var \DateTime
     */
    private $createdDatetime;

    /**
     * @var string|null
     */
    private $documentName;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return [
            'message' => $this->message,
            'messageTranslated' => $this->messageTranslated,
            'type' => $this->type,
            'typeTranslated' => $this->typeTranslated,
            'created_datetime' => $this->createdDatetime,
            'documentName' => $this->documentName
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return LogEntry
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return LogEntry
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessageTranslated(): ?string
    {
        return $this->messageTranslated;
    }

    /**
     * @param string|null $messageTranslated
     * @return LogEntry
     */
    public function setMessageTranslated(?string $messageTranslated): self
    {
        $this->messageTranslated = $messageTranslated;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return LogEntry
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTypeTranslated(): ?string
    {
        return $this->typeTranslated;
    }

    /**
     * @param string|null $typeTranslated
     * @return LogEntry
     */
    public function setTypeTranslated(?string $typeTranslated): self
    {
        $this->typeTranslated = $typeTranslated;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDatetime(): \DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime $createdDatetime
     * @return LogEntry
     */
    public function setCreatedDatetime(\DateTime $createdDatetime): self
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentName(): ?string
    {
        return $this->documentName;
    }

    /**
     * @param string|null $documentName
     * @return LogEntry
     */
    public function setDocumentName(?string $documentName): self
    {
        $this->documentName = $documentName;
        return $this;
    }
}