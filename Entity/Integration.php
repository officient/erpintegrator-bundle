<?php

namespace Officient\ErpIntegrator\Entity;

class Integration implements \JsonSerializable
{
    const TYPE_API = 'api';
    const TYPE_FSC = 'fsc';
    const TYPE_SFTP = 'sftp';

    const TYPES = [self::TYPE_API, self::TYPE_FSC, self::TYPE_SFTP];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $updatedDatetime;

    /**
     * @var \DateTime
     */
    private $createdDatetime;

    /**
     * @var string|null
     */
    private $apiKey;

    /**
     * @var string|null
     */
    private $username;

    /**
     * @var string|null
     */
    private $password;

    /**
     * @var \DateTime|null
     */
    private $lastPingDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return [
            'name' => $this->name,
            'companyId' => $this->companyId,
            'type' => $this->type,
            'active' => $this->active,
            'apiKey' => $this->apiKey,
            'username' => $this->username,
            'password' => $this->password,
            'lastPingDatetime' => $this->lastPingDatetime
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Integration
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Integration
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return Integration
     */
    public function setCompanyId(int $companyId): self
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Integration
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Integration
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDatetime(): \DateTime
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTime $updatedDatetime
     * @return Integration
     */
    public function setUpdatedDatetime(\DateTime $updatedDatetime): self
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDatetime(): \DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime $createdDatetime
     * @return Integration
     */
    public function setCreatedDatetime(\DateTime $createdDatetime): self
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @param string|null $apiKey
     * @return Integration
     */
    public function setApiKey(?string $apiKey): self
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return Integration
     */
    public function setUsername(?string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return Integration
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastPingDatetime(): ?\DateTime
    {
        return $this->lastPingDatetime;
    }

    /**
     * @param \DateTime|null $lastPingDatetime
     * @return Integration
     */
    public function setLastPingDatetime(?\DateTime $lastPingDatetime): self
    {
        $this->lastPingDatetime = $lastPingDatetime;
        return $this;
    }
}