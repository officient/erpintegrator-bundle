<?php

namespace Officient\ErpIntegrator\Entity;

class Document implements \JsonSerializable
{
    const TYPE_DOCUMENT             = 'document';
    const TYPE_APPLICATION_RESPONSE = 'application_response';
    CONST TYPES                     = [self::TYPE_DOCUMENT, self::TYPE_APPLICATION_RESPONSE];

    public function jsonSerialize(): mixed
    {
        return [];
    }
}