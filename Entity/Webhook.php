<?php

namespace Officient\ErpIntegrator\Entity;

class Webhook implements \JsonSerializable
{
    const TYPE_RECEIVE_DOCUMENT             = 'receive_document';
    const TYPE_RECEIVE_APPLICATION_RESPONSE = 'receive_application_response';
    const TYPES                             = [self::TYPE_RECEIVE_DOCUMENT, self::TYPE_RECEIVE_APPLICATION_RESPONSE];

    const CALLBACK_AUTH_METHOD_UNIK = 'unik';
    const CALLBACK_AUTH_METHODS     = [self::CALLBACK_AUTH_METHOD_UNIK];

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string
     */
    private $callbackUrl;

    /**
     * @var string|null
     */
    private $callbackAuthMethod;

    /**
     * @var string|null
     */
    private $callbackAuthParam1;

    /**
     * @var string|null
     */
    private $callbackAuthParam2;

    /**
     * @var string|null
     */
    private $networkType;

    /**
     * @var string|null
     */
    private $networkSchemeId;

    /**
     * @var string|null
     */
    private $networkParticipantId;

    /**
     * @var \DateTimeInterface
     */
    private $updatedDatetime;

    /**
     * @var \DateTimeInterface
     */
    private $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Integration
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return Webhook
     */
    public function setCompanyId(int $companyId): Webhook
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Webhook
     */
    public function setType(string $type): Webhook
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Webhook
     */
    public function setActive(bool $active): Webhook
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getCallbackUrl(): string
    {
        return $this->callbackUrl;
    }

    /**
     * @param string $callbackUrl
     * @return Webhook
     */
    public function setCallbackUrl(string $callbackUrl): Webhook
    {
        $this->callbackUrl = $callbackUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackAuthMethod(): ?string
    {
        return $this->callbackAuthMethod;
    }

    /**
     * @param string|null $callbackAuthMethod
     * @return Webhook
     */
    public function setCallbackAuthMethod(?string $callbackAuthMethod): Webhook
    {
        $this->callbackAuthMethod = $callbackAuthMethod;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackAuthParam1(): ?string
    {
        return $this->callbackAuthParam1;
    }

    /**
     * @param string|null $callbackAuthParam1
     * @return Webhook
     */
    public function setCallbackAuthParam1(?string $callbackAuthParam1): Webhook
    {
        $this->callbackAuthParam1 = $callbackAuthParam1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackAuthParam2(): ?string
    {
        return $this->callbackAuthParam2;
    }

    /**
     * @param string|null $callbackAuthParam2
     * @return Webhook
     */
    public function setCallbackAuthParam2(?string $callbackAuthParam2): Webhook
    {
        $this->callbackAuthParam2 = $callbackAuthParam2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNetworkType(): ?string
    {
        return $this->networkType;
    }

    /**
     * @param string|null $networkType
     * @return Webhook
     */
    public function setNetworkType(?string $networkType): Webhook
    {
        $this->networkType = $networkType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNetworkSchemeId(): ?string
    {
        return $this->networkSchemeId;
    }

    /**
     * @param string|null $networkSchemeId
     * @return Webhook
     */
    public function setNetworkSchemeId(?string $networkSchemeId): Webhook
    {
        $this->networkSchemeId = $networkSchemeId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNetworkParticipantId(): ?string
    {
        return $this->networkParticipantId;
    }

    /**
     * @param string|null $networkParticipantId
     * @return Webhook
     */
    public function setNetworkParticipantId(?string $networkParticipantId): Webhook
    {
        $this->networkParticipantId = $networkParticipantId;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdatedDatetime(): \DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTimeInterface $updatedDatetime
     * @return Webhook
     */
    public function setUpdatedDatetime(\DateTimeInterface $updatedDatetime): Webhook
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDatetime(): \DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTimeInterface $createdDatetime
     * @return Webhook
     */
    public function setCreatedDatetime(\DateTimeInterface $createdDatetime): Webhook
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}